from dotenv import load_dotenv
import os

load_dotenv()

OWM_KEY=os.getenv("owm_key")
TELEGRAM_TOKEN=os.getenv("telegram_token")