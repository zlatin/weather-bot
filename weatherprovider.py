import settings
import pickledb
import pyowm

ODESSA_ID = 698740
DB_FILE = 'weather.db'

db = pickledb.load(DB_FILE, True)
owm = pyowm.OWM(settings.OWM_KEY)

def check_current_weather():
    observation =  owm.weather_at_id(ODESSA_ID)
    observation_time = observation.get_reception_time(timeformat='iso')
    weather = observation.get_weather()
    status = weather.get_detailed_status()
    temperature = weather.get_temperature(unit='celsius')
    wind = weather.get_wind()
    db.set('Odessa', [observation_time, status, int(temperature['temp']), int(wind['speed'])])