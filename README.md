# Simple weather bot

The Telegram bot that fetches current weather conditions from OpenWeatherMap and sends it to Telegram. Currently city is hardcoded to Odessa.

To run bot rename `env.example` to `.env` and past your OpenWeatherMap key and
Telegram token