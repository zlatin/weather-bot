from telegram.ext import Updater, CommandHandler, MessageHandler, JobQueue
from weatherprovider import db, check_current_weather
import datetime
import logging
import settings

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

def start(bot, update):
    WELCOME_TEXT = """Welcome to Odessa weather bot. Send /now for current conditions."""
    update.message.reply_text(WELCOME_TEXT)

def current_weather(bot, update):
    weather_data = db.get("Odessa")
    status, temp, wind = weather_data[1:]
    update.message.reply_text(f"""Current conditions: {status.capitalize()}\nTemperature: {temp}°C\nWind: {wind} m/s""")

def check_weather_callback(bot, job):
    check_current_weather()
    print('checked weather at ', datetime.datetime.now())

def main():
    updater = Updater(settings.TELEGRAM_TOKEN)
    dp = updater.dispatcher

    dp.add_handler(CommandHandler('start', start))
    dp.add_handler(CommandHandler('now', current_weather))
    jobs = updater.job_queue
    jobs.run_repeating(callback=check_weather_callback, interval=30*60, first=0)

    updater.start_polling()
    updater.idle()

if __name__ == "__main__":
    main()